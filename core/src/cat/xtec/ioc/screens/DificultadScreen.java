package cat.xtec.ioc.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import javax.swing.event.ChangeEvent;

import cat.xtec.ioc.SpaceRace;
import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.utils.Settings;

public class DificultadScreen implements Screen, EventListener {

    private Stage stage;
    private SpaceRace game;

    private Label.LabelStyle textStyle;
    private Label textLbl;
    private TextButton facil, medio, dificil;

    public DificultadScreen(SpaceRace game) {

        TextButton.TextButtonStyle estilo = new TextButton.TextButtonStyle();
        estilo.font = AssetManager.font;
        this.game = game;

        boolean touch = false;

        // Creem la càmera de les dimensions del joc
        OrthographicCamera camera = new OrthographicCamera(Settings.GAME_WIDTH, Settings.GAME_HEIGHT);
        // Posant el paràmetre a true configurem la càmera per a
        // que faci servir el sistema de coordenades Y-Down
        camera.setToOrtho(true);

        // Creem el viewport amb les mateixes dimensions que la càmera
        StretchViewport viewport = new StretchViewport(Settings.GAME_WIDTH, Settings.GAME_HEIGHT, camera);

        // Creem l'stage i assginem el viewport
        stage = new Stage(viewport);

        // Afegim el fons
        stage.addActor(new Image(AssetManager.background));

        // Creem la imatge de la nau i li assignem el moviment en horitzontal
        Image spacecraft = new Image(AssetManager.spacecraft);
        float y = Settings.GAME_HEIGHT / 3;
        spacecraft.addAction(Actions.repeat(RepeatAction.FOREVER, Actions.sequence(Actions.moveTo(0 - spacecraft.getWidth(), y), Actions.moveTo(Settings.GAME_WIDTH, y, 5))));

        stage.addActor(spacecraft);

        // Creem l'estil de l'etiqueta i l'etiqueta
        facil = new TextButton("Facil", estilo);
        medio = new TextButton("Medio", estilo);
        dificil = new TextButton("Dificil", estilo);

        // Creem el contenidor necessari per aplicar-li les accions
        Container containerFacil = new Container(facil);
        containerFacil.setTransform(true);
        containerFacil.center();

        containerFacil.setPosition(Settings.GAME_WIDTH/2+10, 30);
        Container containerMedio = new Container(medio);
        containerMedio.setTransform(true);
        containerMedio.center();

        containerMedio.setPosition(Settings.GAME_WIDTH/2+10, 60);
        Container containerDificil = new Container(dificil);
        containerDificil.setTransform(true);
        containerDificil.center();

        containerDificil.setPosition(Settings.GAME_WIDTH/2+10, 90);
        stage.addActor(containerFacil);
        stage.addActor(containerMedio);
        stage.addActor(containerDificil);

        facil.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                DificultadScreen.this.game.setScreen(new GameScreen(DificultadScreen.this.stage.getBatch(), DificultadScreen.this.stage.getViewport(), "facil"));
                dispose();
            }
        });

        medio.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                DificultadScreen.this.game.setScreen(new GameScreen(DificultadScreen.this.stage.getBatch(), DificultadScreen.this.stage.getViewport(), "medio"));
                dispose();
            }
        });

        dificil.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                DificultadScreen.this.game.setScreen(new GameScreen(DificultadScreen.this.stage.getBatch(), DificultadScreen.this.stage.getViewport(), "dificil"));
                dispose();
            }
        });

        Gdx.input.setInputProcessor(stage);
    }
    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        stage.draw();
        stage.act(delta);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public boolean handle(Event event) {
        return false;
    }
}
